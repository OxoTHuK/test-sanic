FROM python:3.6

COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt

ENV SANIC_APP_HOST="0.0.0.0"
ENV SANIC_APP_PORT=8000
ENV SANIC_SMTP_HOST="smtp.yandex.ru"
ENV SANIC_SMTP_PORT=465

CMD ["python", "./index.py"]

EXPOSE 8000