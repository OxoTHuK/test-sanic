class MainFormDto:

    def __init__(self, email_from: str, password: str, email_to: str, message: str) -> None:
        self.email_from = email_from
        self.password = password
        self.email_to = email_to
        self.message = message
