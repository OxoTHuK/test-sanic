from .main_from_dto import MainFormDto
from .main_form_validate_result import MainFormValidateResult
from validate_email import validate_email


class MainFormValidator:

    def validate_form(self, form_data: MainFormDto) -> MainFormValidateResult:
        error_list = []
        if not form_data.email_from:
            error_list.append("Email отправителя должен быть заполнен")
        else:
            if not validate_email(form_data.email_from):
                error_list.append("Некорректный email отправителя")

            split_email_from = form_data.email_from.split('@')
            if split_email_from[-1] != "yandex.ru" and split_email_from[-1] != "ya.ru":
                error_list.append("Email отправителя должен быть на яндексе")

        if not form_data.email_to:
            error_list.append("Email получателя должен быть заполнен")
        else:
            if not validate_email(form_data.email_to):
                error_list.append("Некорректный email получателя")

        if not form_data.password:
            error_list.append("Пароль от email отправителя должен быть заполнен")

        if not form_data.message:
            error_list.append("Сообщение должно быть заполнено")

        return MainFormValidateResult(not error_list, error_list)
