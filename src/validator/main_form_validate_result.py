class MainFormValidateResult:

    def __init__(self, result: bool, error_list: list) -> None:
        self.result = result
        self.error_list = error_list
