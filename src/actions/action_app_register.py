from sanic import Sanic
from .main_action import MainAction
from .process_form_action import ProcessFormAction


class ActionAppRegister:

    def register(self, app: Sanic):
        app.add_route(MainAction.as_view(), '/')
        app.add_route(ProcessFormAction.as_view(), '/send')
