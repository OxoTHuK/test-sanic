from sanic.views import HTTPMethodView
from sanic.request import Request
from sanic.response import redirect
from ..mailer import MessageDTO, MailAttachFileDTO, AuthError, SendError
from ..validator import MainFormDto, MainFormValidator


class ProcessFormAction(HTTPMethodView):

    async def post(self, request: Request):
        url = request.app.url_for('MainAction')

        email_from = request.form.get('email_from')
        password = request.form.get('password')
        email_to = request.form.get('email_to')
        message = request.form.get('message')
        attach_file = request.files.get('attach_file')
        form_data = {
            'email_from': email_from,
            'email_to': email_to,
            'message': message
        }

        validator = MainFormValidator()
        result = validator.validate_form(MainFormDto(email_from, password, email_to, message))
        if not result.result:
            request['session']['result'] = False
            request['session']['error_list'] = result.error_list
            request['session']['form_data'] = form_data
            return redirect(url)

        mail_message = MessageDTO(email_from, password, email_from, email_to, message)
        if attach_file and attach_file.body:
            mail_message.attach_file = MailAttachFileDTO(attach_file.name, attach_file.body)

        mailer = request.app.mailer
        try:
            await mailer.send_message(mail_message)
            request['session']['result'] = True
        except AuthError:
            request['session']['result'] = False
            request['session']['error_list'] = ["Не верные логин или пароль"]
            request['session']['form_data'] = form_data
        except SendError:
            request['session']['result'] = False
            request['session']['error_list'] = ["Не удалось отправить сообщение"]
            request['session']['form_data'] = form_data
        return redirect(url)
