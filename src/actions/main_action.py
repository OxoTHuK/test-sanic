from sanic.views import HTTPMethodView
from sanic.request import Request
import jinja2_sanic


class MainAction(HTTPMethodView):

    @jinja2_sanic.template("index.html")
    async def get(self, request: Request):
        result = request['session'].get('result')
        error_list = request['session'].get('error_list')
        form_data = request['session'].get('form_data')
        if result:
            del request['session']['result']
        if error_list:
            del request['session']['error_list']
        if form_data:
            del request['session']['form_data']
        return {'result': result, 'error_list': error_list, 'form_data': form_data}
