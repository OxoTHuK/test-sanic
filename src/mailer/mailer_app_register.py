from sanic import Sanic
import aiosmtplib
from .mailer import Mailer

class MailerAppRegister:

    def register(self, app: Sanic):

        def setup_mailer(app: Sanic, loop):
            smtp_host = app.config.get('SMTP_HOST')
            if not smtp_host:
                smtp_host = "smtp.yandex.ru"
            smtp_port = app.config.get('SMTP_PORT')
            if not smtp_port:
                smtp_port = 465
            smtp_client = aiosmtplib.SMTP(smtp_host, int(smtp_port), loop=loop, use_tls=True)
            app.mailer = Mailer(smtp_client)

        app.register_listener(setup_mailer, 'before_server_start')
