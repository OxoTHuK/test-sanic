class MailAttachFileDTO:

    def __init__(self, file_name: str, file_content) -> None:
        self.file_name = file_name
        self.file_content = file_content
