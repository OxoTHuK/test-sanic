from aiosmtplib import SMTP, SMTPAuthenticationError
from .message_dto import MessageDTO
from .auth_error import AuthError
from .send_error import SendError
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication


class Mailer:

    def __init__(self, smtp_client: SMTP) -> None:
        self._smtp_client = smtp_client

    async def send_message(self, message: MessageDTO) -> None:
        try:
            await self._smtp_client.connect(timeout=10)
            try:
                await self._smtp_client.login(message.login, message.password)
                email_message = MIMEMultipart("alternative")
                email_message['Subject'] = 'Письмо от %s' % message.email_from
                email_message['From'] = message.email_from
                email_message['To'] = message.email_to
                email_message.attach(MIMEText(message.message, "plain", "utf-8"))
                if message.attach_file:
                    attachment = MIMEApplication(message.attach_file.file_content, Name=message.attach_file.file_name)
                    attachment.add_header('Content-Disposition', 'attachment', filename=message.attach_file.file_name)
                    email_message.attach(attachment)
                await self._smtp_client.send_message(email_message)
            except SMTPAuthenticationError:
                raise AuthError
            except Exception:
                raise SendError
            finally:
                self._smtp_client.close()
        except (AuthError, SendError):
            raise
        except Exception:
            raise SendError
