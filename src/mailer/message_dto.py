from .mail_attach_file_dto import MailAttachFileDTO


class MessageDTO:

    def __init__(self, login: str, password: str, email_from: str, email_to: str, message: str,
                 attach_file: MailAttachFileDTO = None) -> None:
        self.login = login
        self.password = password
        self.email_from = email_from
        self.email_to = email_to
        self.message = message
        self.attach_file = attach_file
