from sanic import Sanic
import jinja2
import jinja2_sanic
from sanic_session import Session
from sanic_session import InMemorySessionInterface
from src.mailer import MailerAppRegister
from src.actions import ActionAppRegister

app = Sanic("Test src")
jinja2_sanic.setup(
    app,
    loader=jinja2.FileSystemLoader('./templates')
)
Session(app, interface=InMemorySessionInterface())

mailer_app_register = MailerAppRegister()
mailer_app_register.register(app)

action_app_register = ActionAppRegister()
action_app_register.register(app)


if __name__ == '__main__':
    app_host = app.config.get('APP_HOST')
    if not app_host:
        app_host = "0.0.0.0"
    app_port = app.config.get('APP_PORT')
    if not app_port:
        app_port = 8000

    app.run(host=app_host, port=app_port)